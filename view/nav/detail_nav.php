<div class="container-fluid" >
    <section class="content-header">
        <h5 class="text-primary">
            <i class="fa fa-link fa-2x"></i> Nav detail
        </h5>


    </section> 
    <section class='content'>
        <div class="row">
            <div class="col-xl-6 col-lg-5">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-gray-100">
                        <h6 class="m-0 font-weight-bold text-primary"> 
                            <a href="/qcontent/Nav/index" class="btn btn-info btn-circle" title="volver">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                            <span id="detail_title">  <?php echo $Nav->navname; ?> </span>
                        </h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="top-end" >
                                <div class="dropdown-header">Opciones:</div>

                                <a href="#" class="dropdown-item"
                                   data-target="#modalLg" 
                                   data-remote="/qcontent/Nav/edit/<?php echo $id_nav; ?>"
                                   data-toggle="modal"
                                   data-titlemodal='editar Nav'
                                   >
                                    <span class="icon">
                                        <i class="fas fa-edit"></i>
                                    </span>
                                    <span class="text">Editar</span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="/qcontent/Nav/delete/<?php echo $id_nav; ?>">
                                    <span class="icon">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                    <span class="text">Eliminar</span>    
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">

                        <div class="container-fluid small" id="data_Nav" data-Nav="<?php echo $id_nav; ?>">
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Title</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_navname">
                                    <?php echo $Nav->navname; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Parent nav</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_parent_nav">
                                    <?php echo $Nav->parent_nav; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Target</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_target">
                                    <?php echo $Nav->target; ?>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Url</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_url">
                                    <a href="<?php echo $Nav->url; ?>" target="<?php echo $Nav->target; ?>" title="<?php echo $Nav->navname; ?>">
                                        <?php echo $Nav->url; ?>
                                    </a>
                                </div>
                            </div>
                            <div class="row m-1">
                                <div class="col-sm-3 bg-gray-100 p-2">
                                    <b>Position</b>
                                </div>
                                <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_position">
                                    <?php echo $Nav->position; ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-6 col-lg-7">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Childs</h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                <div class="dropdown-header">Opciones:</div>
                                <a class="dropdown-item" href="#">Opcion1</a>
                                <a class="dropdown-item" href="#">Opcion2</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Otra opcion mas</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <table class="table table-sm table-hover">
                            <thead class="thead-light">
                                <tr>
                                    <th></th>
                                    <th>Item</th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php foreach($Childs AS $Child):?>
                                <tr>
                                    <td>
                                        <a href="/qcontent/nav/detail/<?php echo $Child->id_nav;?>"
                                           class="btn btn-default btn-sm">
                                            <i class="fa fa-tasks"></i>
                                        </a>
                                    </td>
                                    <td><?php echo $Child->navname;?></td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                            
                        </table>
                        
                        
                        
                        
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {

    });


</script>