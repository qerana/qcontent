<div class="container-fluid">

    <section class="content-header">
        <h5 class="text-primary">
            <i class="fa fa-link fa-2x"></i> Nav List
        </h5>


    </section> 


    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <a href="#" class="btn btn-default btn-success"
               data-target="#modalLg" 
               data-remote="/qcontent/Nav/add"
               data-toggle="modal"
               data-titlemodal='New NavItem'
               >
                <span class="icon">
                    <i class="fas fa-plus-square"></i>
                </span>
                <span class="text">Add</span>
            </a>

            <div class="dropdown no-arrow">
                <h6 class="m-0 font-weight-bold">
                    Total:<span class="badge" id="total"></span>
                </h6>
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            <ol class="breadcrumb">
                <li>
                    <input type="text" class="form-control input-sm" 
                           id="filter_search" 
                           name="filter_search"
                           aria-describedby="inputSuccess3Status"
                           value="" placeholder="Simple Search"/>
                </li>
            </ol>
            <section id='results' class='table-responsive'>
                <div id="loader"></div>
                <table class="table_search table table-dark table-hover table-striped">
                    <thead class='thead-light'>
                        <tr class="bg-primary">
                            <td class=""></td>
                            <td>Navname</td>
                            <td>Parent Nav</td>
                            <td>Target</td>
                            <td>Url</td>
                            <td>Position</td>


                        </tr>
                    </thead>
                    <tbody class="small" id="list_Nav">
                        <!-- via json ajax -->
                    </tbody>
                </table>
            </section>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        loadNav();



// search html data


    });

    $("#filter_search").keyup(function () {
        var filter = $(this).val();

        $("table.table_search > tbody > tr").each(function () {

            // si la lista no existe el string a mostrar, ocultamos las filas
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();

                // Si coincide mostramos el tr
            } else {
                $(this).show();

            }
        });

    });

</script>