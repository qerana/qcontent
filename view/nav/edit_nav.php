<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qcontent/Nav/save" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_id_nav" id="f_id_nav" value="<?php echo $Nav->id_nav; ?>">
                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Save</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancel
                    </button>
                </header>

                <div class='form-group form-group-sm row small'> 
                    <label for='f_parent_nav' class='col-sm-3 col-form-label'>Parent</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   

                            <select name="f_parent_nav" id="f_parent_nav" class="form-control form-control-sm">
                                <option value="0" <?php echo ($Nav->parent_nav == '0') ? 'selected' : ''; ?>
                                        >--Top--</option>
                                        <?php foreach ($Parents AS $Parent): ?>
                                    <option value="<?php echo $Parent->id_nav; ?>"
                                            <?php echo ($Nav->parent_nav == $Parent->id_nav) ? 'selected' : ''; ?>>
                                                <?php echo $Parent->navname; ?>
                                    </option>

                                <?php endforeach; ?>
                            </select>
                        </div>   
                    </div>   
                </div>   

                <div class='form-group form-group-sm row small'> 
                    <label for='f_target' class='col-sm-3 col-form-label'>Target</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-5'>   

                            <select name="f_target" id="f_target" required class="form-control form-control-sm">
                                <?php foreach ($targets AS $target): ?>
                                    <option value="<?php echo $target; ?>"
                                            <?php echo ($Nav->target == $target) ? 'selected' : ''; ?>>
                                                <?php echo $target; ?>
                                    </option>
                                <?php endforeach; ?>
                            </select>
                        </div>   
                    </div>   
                </div>   


                <div class='form-group form-group-sm row small'> 
                    <label for='f_navname' class='col-sm-3 col-form-label'>Title</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_navname' name='f_navname' 

                                   class='form-control form-control-sm'    value='<?php echo $Nav->navname; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_url' class='col-sm-3 col-form-label'>Url</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_url' name='f_url' 

                                   class='form-control form-control-sm' required   value='<?php echo $Nav->url; ?>' />
                        </div>   
                    </div>   
                </div>   
                <div class='form-group form-group-sm row small'> 
                    <label for='f_position' class='col-sm-3 col-form-label'>Position<?php echo $Nav->position; ?></label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   

                            <?php foreach ($positions AS $position): ?>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="checkbox" 
                                           name="array_position[]" id="pos_<?php echo $position; ?>"
                                           value="<?php echo $position; ?>"
                                           <?php echo (in_array($position, $Nav->getPositions())) ? 'checked' : ''; ?>>
                                    <label class="form-check-label" for="inlineCheckbox1"><?php echo $position; ?></label>
                                </div>
                            <?php endforeach; ?>
                        </div>   
                    </div>   
                </div>   

                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Save</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancel
                    </button>
                </header>

            </form>
        </div>
    </div>
</div>
<script>



</script>


