<div class="container-fluid">

    <section class="content-header">
        <h5 class="text-primary">
            <i class="fa fa-cloud fa-2x"></i> Page List
        </h5>
    </section> 


    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold">
                <a href="#" class="btn btn-success"
                   data-target="#modalLg" 
                   data-remote="/qcontent/Pagemanagement/add"
                   data-toggle="modal"
                   data-titlemodal='Add page'
                   >
                    <span class="icon">
                        <i class="fas fa-plus-square"></i>
                    </span>
                    <span class="text">Add</span>
                </a>
            </h6>
            <div class="dropdown no-arrow">
                total:<span class="badge" id="total">
                    <?php echo count($Pages); ?>
                </span>
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            <section id='results' class='table-responsive'>

                    <div class="row mb-2">
                        <?php foreach ($Pages AS $Page): ?>

                            <div class="col-md-4">
                                <div class="card flex-md-row mb-4 box-shadow h-md-250">
                                    <div class="card-body d-flex flex-column align-items-start">
                                        <strong class="d-inline-block mb-2 text-primary">
                                            <?php echo $Page->getType(); ?>
                                        </strong>
                                        <h3 class="mb-0">
                                            <a class="text-dark" href="#">
                                                <?php echo $Page->title; ?>
                                            </a>
                                        </h3>
                                        <p class="card-text mb-auto">
                                            <?php echo $Page->meta; ?>
                                        </p>
                                        <a href="/qcontent/pagemanagement/detail/<?php echo $Page->id_page; ?>"
                                           class="btn btn-info">Detail</a>
                                    </div>
                                    
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </div>                


            </section>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {



// search html data


    });

    $("#filter_search").keyup(function () {
        var filter = $(this).val();

        $("table.table_search > tbody > tr").each(function () {

            // si la lista no existe el string a mostrar, ocultamos las filas
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();

                // Si coincide mostramos el tr
            } else {
                $(this).show();

            }
        });

    });

</script>