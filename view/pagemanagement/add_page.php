<div class="modal-body">
    <div class="card shadow mb-4">
        <div class="card-body">
            <form action="/qcontent/Pagemanagement/save" 
                  id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                  accept-charset="utf-8">
                <input type="hidden" name="f_html_content" value="write your content">
                <input type="hidden" name="f_meta" value="write your meta">
                <input type="hidden" name="f_keywords" value="write your keywords">
                <input type="hidden" name="f_namespace_destiny" value="destiny">

                <?php echo $kerana_token; ?>
                <header class="breadcrumb">

                    <button type="submit" class="btn btn-success btn-sm">Save</button> &nbsp;
                    <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                            aria-label="Close">
                        Cancel
                    </button>
                </header>

                <div class='form-group form-group-sm row small'> 
                    <label for='f_type' class='col-sm-3 col-form-label'>Type</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <select name="f_type" id="f_type" class="form-control form-control-sm" required>
                                <option value="">-Select type-</option>
                                <?php foreach ($types AS $type): ?>
                                    <option value="<?php echo $type; ?>"><?php echo $type; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>   
                    </div>   
                </div> 
                <div class='form-group form-group-sm row small'> 
                    <label for='f_parent_page' class='col-sm-3 col-form-label'>Parent</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <select name="f_parent_page" id="f_parent_page" class="form-control form-control-sm" required>
                                <option value="0">-Top-</option>
                                <?php foreach ($Pages AS $Page): ?>
                                    <option value="<?php echo $Page->id_page; ?>">
                                        <?php echo $Page->title; ?>
                                    </option>

                                <?php endforeach; ?>
                            </select>
                        </div>   
                    </div>   
                </div> 
                <div class='form-group form-group-sm row small'> 
                    <label for='f_title' class='col-sm-3 col-form-label'>Title</label>  
                    <div class='col-sm-9'>  
                        <div class='input-group col-sm-8'>   
                            <input type='text' id='f_title' name='f_title' 

                                   class='form-control form-control-sm' required   />
                        </div>   
                    </div>   
                </div>   


            </form>
        </div>
    </div>
</div>
<script>



</script>


