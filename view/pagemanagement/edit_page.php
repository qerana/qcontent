<script src="https://cloud.tinymce.com/stable/tinymce.min.js?apiKey=vsnb1rt4r5aaahhfaaay7o499r648y37aqr7kan70j6nqq5o"></script> 

<script>
    tinymce.init({
        selector: '#f_html_content',
        height: 500,
        theme: 'modern',
        force_p_newlines: false,
        force_br_newlines: true,
        menubar: true,
        statusbar: true,
        forced_root_block: '',
        plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
        imagetools_cors_hosts: ['picsum.photos'],
        menubar: 'file edit view insert format tools table help',
        toolbar: 'undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist | forecolor backcolor removeformat | pagebreak | charmap emoticons | fullscreen  preview save print | insertfile image media template link anchor codesample | ltr rtl',
        toolbar_sticky: true,
        autosave_ask_before_unload: true,
        autosave_interval: "30s",
        autosave_prefix: "{path}{query}-{id}-",
        autosave_restore_when_empty: false,
        autosave_retention: "2m",
        image_advtab: true,
        templates: [
            {title: 'Test template 1', content: 'Test 1'},
            {title: 'Test template 2', content: 'Test 2'}
        ],
        content_css: [
            '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
            '/_styles/qcontent/templates/unapp/css/animate.css',
            '/_styles/qcontent/templates/unapp/css/icomoon.css',
            '/_styles/qcontent/templates/unapp/css/bootstrap.css',
            '/_styles/qcontent/templates/unapp/css/magnific-popup.css',
            '/_styles/qcontent/templates/unapp/css/owl.carousel.min.css',
            '/_styles/qcontent/templates/unapp/css/owl.theme.default.min.css',
            '/_styles/qcontent/templates/unapp/css/style.css'
        ]
    });
</script>
<div class="container-fluid" >

    <section class="content-header">
        <h6 class="text-black">
            <i class="fa fa-edit fa-2x"></i>Edit Page  <b><?php echo $Page->title; ?></p>

        </h6>
    </section> 
    <section class='content'>
        <div class="rows">
            <div class="card shadow mb-4">
                <div class="card-body">
                    <form action="/qcontent/Pagemanagement/save" 
                          id="formQerana" name="formQerana" method="POST" class="form-horizontal"
                          accept-charset="utf-8">
                        <input type="hidden" name="f_id_page" id="f_id_page" value="<?php echo $Page->id_page; ?>">
                        <?php echo $kerana_token; ?>
                        <header class="breadcrumb">

                            <button type="submit" class="btn btn-success btn-sm">Save</button> &nbsp;
                            <a href="/qcontent/pagemanagement/detail/<?php echo $Page->id_page; ?>"
                               class="btn btn-warning btn-sm" >
                                Cancel
                            </a>
                        </header>
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_type' class='col-sm-3 col-form-label'>Type</label>  
                            <div class='col-sm-9'>  
                                <div class='input-group col-sm-4'>   
                                    <select name="f_type" id="f_type" class="form-control form-control-sm" required>
                                        <?php foreach ($types AS $type): ?>
                                            <option value="<?php echo $type; ?>"
                                            <?php echo ($type == $Page->type) ? 'selected' : ''; ?>
                                                    >
                                                        <?php echo $type; ?>
                                            </option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>   
                            </div>   
                        </div> 
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_parent_page' class='col-sm-3 col-form-label'>Parent<?php echo $Page->parent_page; ?></label>  
                            <div class='col-sm-9'>  
                                <div class='input-group col-sm-6'>   
                                    <select name="f_parent_page" id="f_parent_page" class="form-control form-control-sm" required>
                                        <option value="0"
                                                <?php echo ($Page->parent_page == '0') ? 'selected' : ''; ?>>-Top-</option>
                                                <?php foreach ($Pages AS $PageParent): ?>
                                            <option value="<?php echo $PageParent->id_page; ?>"
                                            <?php echo ($PageParent->id_page == $Page->parent_page) ? 'selected' : ''; ?>
                                                    >
                                                        <?php echo $PageParent->title; ?>
                                            </option>

                                        <?php endforeach; ?>
                                    </select>
                                </div>   
                            </div>   
                        </div> 
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_title' class='col-sm-3 col-form-label'>Title</label>  
                            <div class='col-sm-9'>  
                                <div class='input-group col-sm-8'>   
                                    <input type='text' id='f_title' name='f_title' 

                                           class='form-control form-control-sm' required   value='<?php echo $Page->title; ?>' />
                                </div>   
                            </div>   
                        </div>   
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_meta' class='col-sm-3 col-form-label'>Meta</label>  
                            <div class='col-sm-9'>  
                                <div class='input-group col-sm-8'>   
                                    <textarea id='f_meta' name='f_meta' 
                                              class='form-contro form-control-sml' cols='60' rows='5'><?php echo $Page->meta; ?></textarea>
                                </div>   
                            </div>   
                        </div>   
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_keywords' class='col-sm-3 col-form-label'>Keywords</label>  
                            <div class='col-sm-9'>  
                                <div class='input-group col-sm-8'>   
                                    <textarea id='f_keywords' name='f_keywords' 
                                              class='form-contro form-control-sml' cols='40' rows='2'><?php echo $Page->keywords; ?></textarea>
                                </div>   
                            </div>   
                        </div>   
                        <div class='form-group form-group-sm row small'> 
                            <label for='f_layout' class='col-sm-3 col-form-label'>Layout</label>  
                            <div class='col-sm-9'>  
                                <div class='input-group col-sm-8'>   
                                    <input type='text' id='f_layout' name='f_layout' 

                                           class='form-control form-control-sm'    value='<?php echo $Page->layout; ?>' />
                                </div>   
                            </div>   
                        </div>   
                        <div class="card-body">
                            <textarea id='f_html_content' name='f_html_content' required
                                      class='form-control form-control-sml' cols='60' rows='5'><?php echo $Page->html_content; ?></textarea>
                        </div>

                        <header class="breadcrumb">

                            <button type="submit" class="btn btn-success btn-sm">Save</button> &nbsp;
                            <button type="button" class="btn btn-warning btn-sm" data-dismiss="modal"
                                    aria-label="Close">
                                Cancel
                            </button>
                        </header>

                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

