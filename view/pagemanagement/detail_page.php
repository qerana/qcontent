<div class="container-fluid" >

    <section class="content-header">
        <h6 class="text-black">
            Page  <b><?php echo $Page->title; ?></p>

        </h6>
    </section> 

    <section class='content'>
        <div class="row">
            <div class="col-xl-9 col-lg-8">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-gray-100">
                        <h6 class="m-0 font-weight-bold text-primary"> 
                            <a href="/qcontent/pagemanagement/index" class="btn btn-dark btn-circle" title="volver">
                                <i class="fa fa-arrow-left"></i>
                            </a>

                            <a href="/qcontent/pagemanagement/edit/<?php echo $id_page; ?>"
                               class="btn btn-outline-primary">
                                <span class="icon">
                                    <i class="fas fa-edit"></i>
                                </span>
                                <span class="text">Editar</span>
                            </a>
                            <a class="btn  btn-outline-danger" href="/qcontent/pagemanagement/delete/<?php echo $id_page; ?>">
                                <span class="icon">
                                    <i class="fas fa-trash"></i>
                                </span>
                                <span class="text">Eliminar</span>    
                            </a>
                        </h6>
                        <div class="dropdown no-arrow">
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" 
                                    src="<?php echo __URL__ . '/qcontent/page/view/' . $Page->title . '.html'; ?>" 
                                    allowfullscreen height="100%"></iframe>
                        </div>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-3 col-lg-4">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">Properties</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                        <div class="row m-1">
                            <div class="col-sm-9 border border-1 border-top-0 border-right-0 small">
                               <?php echo $Page->getType(); ?>
                            </div>
                        </div>
                        <div class="row m-1">
                            <div class="col-sm-9 border border-1 border-top-0 border-right-0 small">
                                <?php echo $Page->namespace_destiny; ?>
                            </div>
                        </div>


                    </div>
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">SeoInfo</h6>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">

                        <h6 class="font-weight-bold">MetaDescription</h6>
                        <p class="font-weight-normal small">
                            <?php echo $Page->meta; ?>
                        </p>
                        <h6 class="font-weight-bold">Keywords</h6>
                        <p class="font-weight-normal small">
                            <?php echo $Page->keywords; ?>
                        </p>


                    </div>


                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {

    });


</script>