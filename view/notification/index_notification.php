<div class="container-fluid">

    <section class="content-header">
        <h5 class="text-primary">
            <i class="fa fa-folder fa-2x"></i> Notification List
        </h5>
        <ol class="breadcrumb">
            <li>
                <input type="text" class="form-control input-sm" 
                       id="filter_search" 
                       name="filter_search"
                       aria-describedby="inputSuccess3Status"
                       value="" placeholder="Buscador"/>
            </li>
        </ol>

    </section> 


    <div class="card shadow mb-4">
        <!-- Card Header - Dropdown -->
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold">
                total:<span class="badge" id="total"></span>
            </h6>
            <div class="dropdown no-arrow">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                    <div class="dropdown-header">Opciones:</div>
                    <a href="#" class="dropdown-item"
                       data-target="#modalLg" 
                       data-remote="/qcontent/Notification/add"
                       data-toggle="modal"
                       data-titlemodal='Nuevo Notification'
                       >
                       <span class="icon">
                                        <i class="fas fa-plus-square"></i>
                                    </span>
                                    <span class="text">Nuevo</span>
                    </a>
                    <a class="dropdown-item" href="#">Opcion2</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">Otra opcion mas</a>
                </div>
            </div>
        </div>
        <!-- Card Body -->
        <div class="card-body">
            <section id='results' class='table-responsive'>
                <div id="loader"></div>
                <table class="table_search table table-bordered table-condensed table-hover">
                    <thead class='bg-gray-600 text-white'>
                        <tr class="small">
                        <td class="bg-gray-800"></td>
                             <td>Id Notification</td>
<td>Id Page</td>
<td>Title Notification</td>
<td>Content Notification</td>
<td>Expiration Date</td>


                        </tr>
                    </thead>
                    <tbody class="small" id="list_Notification">
                        <!-- via json ajax -->
                    </tbody>
                </table>
            </section>
        </div>
    </div>

</div>
<script>
    $(document).ready(function () {
        loadNotification();



// search html data


    });

    $("#filter_search").keyup(function () {
        var filter = $(this).val();

        $("table.table_search > tbody > tr").each(function () {

            // si la lista no existe el string a mostrar, ocultamos las filas
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();

                // Si coincide mostramos el tr
            } else {
                $(this).show();

            }
        });

    });

</script>