<div class="container-fluid" >

    <section class="content-header">
        <h6 class="text-black">
            Detalle Notification
            
        </h6>
    </section> 

    <section class='content'>
        <div class="row">
            <div class="col-xl-8 col-lg-7">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between bg-gray-100">
                        <h6 class="m-0 font-weight-bold text-primary"> 
                            <a href="/qcontent/Notification/index" class="btn btn-info btn-circle" title="volver">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                             <span id="detail_title">  </span>
                        </h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink" x-placement="top-end" >
                                <div class="dropdown-header">Opciones:</div>

                                <a href="#" class="dropdown-item"
                                   data-target="#modalLg" 
                                   data-remote="/qcontent/Notification/edit/<?php echo $id_notification; ?>"
                                   data-toggle="modal"
                                   data-titlemodal='editar Notification'
                                   >
                                    <span class="icon">
                                        <i class="fas fa-edit"></i>
                                    </span>
                                    <span class="text">Editar</span>
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item text-danger" href="/qcontent/Notification/delete/<?php echo $id_notification; ?>">
                                    <span class="icon">
                                        <i class="fas fa-trash"></i>
                                    </span>
                                    <span class="text">Eliminar</span>    
                                </a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">

                        <div class="container-fluid small" id="data_Notification" data-Notification="<?php echo $id_notification; ?>">
                             <div class="row m-1">
 <div class="col-sm-3 bg-gray-100 p-2">
         <b>id notification</b>
 </div>
    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_id_notification">
        <?php echo $Notification->id_notification;?>
    </div>
</div>
 <div class="row m-1">
 <div class="col-sm-3 bg-gray-100 p-2">
         <b>id page</b>
 </div>
    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_id_page">
        <?php echo $Notification->id_page;?>
    </div>
</div>
 <div class="row m-1">
 <div class="col-sm-3 bg-gray-100 p-2">
         <b>title notification</b>
 </div>
    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_title_notification">
        <?php echo $Notification->title_notification;?>
    </div>
</div>
 <div class="row m-1">
 <div class="col-sm-3 bg-gray-100 p-2">
         <b>content notification</b>
 </div>
    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_content_notification">
        <?php echo $Notification->content_notification;?>
    </div>
</div>
 <div class="row m-1">
 <div class="col-sm-3 bg-gray-100 p-2">
         <b>expiration date</b>
 </div>
    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_expiration_date">
        <?php echo $Notification->expiration_date;?>
    </div>
</div>

                        </div>
                    </div>
                </div>
            </div>

            <!-- Pie Chart -->
            <div class="col-xl-4 col-lg-5">
                <div class="card shadow mb-4">
                    <!-- Card Header - Dropdown -->
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold text-primary">title</h6>
                        <div class="dropdown no-arrow">
                            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                                <div class="dropdown-header">Opciones:</div>
                                <a class="dropdown-item" href="#">Opcion1</a>
                                <a class="dropdown-item" href="#">Opcion2</a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#">Otra opcion mas</a>
                            </div>
                        </div>
                    </div>
                    <!-- Card Body -->
                    <div class="card-body">
                    </div>
                </div>
            </div>
        </div>

    </section>
</div>
<script>
    $(document).ready(function () {
       
    });


</script>