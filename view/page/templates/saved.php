<div class="card shadow mb-4">
    <!-- Card Header - Dropdown -->
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold">
            total:<span class="badge" id="total"></span>
        </h6>
        <div class="dropdown no-arrow">
            <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                <div class="dropdown-header">Opciones:</div>
                <a href="#" class="dropdown-item"
                   data-target="#modalLg" 
                   data-remote="/qemail/Template/add"
                   data-toggle="modal"
                   data-titlemodal='Nuevo Template'
                   >
                    <span class="icon">
                        <i class="fas fa-plus-square"></i>
                    </span>
                    <span class="text">Nuevo</span>
                </a>
                <a class="dropdown-item" href="#">Opcion2</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Otra opcion mas</a>
            </div>
        </div>
    </div>
    <!-- Card Body -->
    <div class="card-body">
        <section id='results' class='table-responsive'>
            <div id="loader"></div>
            <table class="table_search table table-bordered 
                   table-hover table-condensed table-hover">
                <thead class='bg-primary text-white'>
                    <tr class="small">
                        <td>Template</td>
                        <td>Type</td>
                        <td>Active</td>
                    </tr>
                </thead>
                <tbody class="small" id="">
                    <?php foreach($Templates AS $Template):?>
                    <tr>
                        <td><?php echo $Template->tpl_name;?></td>
                        <td><?php echo $Template->tpl_type;?></td>
                        <td><?php echo $Template->tpl_active;?></td>
                    </tr>
                    <?php endforeach;?>
                </tbody>
            </table>
        </section>
    </div>
</div>