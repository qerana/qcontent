

<section class="pb_cover_v3 overflow-hidden cover-bg-indigo cover-bg-opacity text-left pb_gradient_v1 pb_slant-light" id="section-home1">
    <div class="container" style="">
        <div class="row align-items-center justify-content-center">
            <div class="row">
                <h1><?php echo $Page->title; ?></h1>
                <div class="card shadow mb-4">
                    <div class="card-body">
                        <p>
                            <?php echo $Page->html_content; ?>
                        </p>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>

