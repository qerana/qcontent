<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content;

use Qerapp\qcontent\model\content\entity\NavEntity,
    Qerapp\qcontent\model\content\mapper\NavMapper,
    Qerapp\qcontent\model\content\interfaces\NavMapperInterface,
    Qerapp\qcontent\model\content\repository\NavRepository;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity NavService
  | @author qDevTools,
  | @date 2020-03-07 07:49:31,
  |*****************************************************************************
 */

class NavService {

    public
    //RELATED-MAPPER-OBJECT
            $NavRepository,
            $Navs,
            /** @object entity NAV */
            $Nav,
            $id_nav,
            $targets = ['_self','_parent','_blank'],
            $positions = ['top','left','right','bottom'];

    public function __construct(NavMapperInterface $Mapper = null) {

        //RELATED-MAPPER-OBJECT-NEW



        try {
            $NavMapper = new NavMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.NavService', $ex);
        }

        $MapperRepository = (is_null($Mapper)) ? $NavMapper : $Mapper;
        $this->NavRepository = new NavRepository($MapperRepository);
    }

    /**
     * -------------------------------------------------------------------------
     * simple test
     * -------------------------------------------------------------------------
     */
    public function runTest() {
      
    }

    /**
     * Get nav menu
     * @param type $parent
     * @return type
     */
    public function getNavMenu($parent = 0) {

        $ChildRepo = new NavRepository(new NavMapper);
        $Navs = $ChildRepo->findByParent_nav($parent);

        $nav_array = [];

        foreach ($Navs AS $k => $Nav):
            $Nav->Childs = $this->getNavMenu($Nav->id_nav);
            $nav_array[] = $Nav;
        endforeach;

        return $nav_array;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false) {

        $Collection = $this->NavRepository->findAll();
        if ($json) {
            echo json_encode($Collection);
        } else {
            return $Collection;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false) {

        $Entity = $this->NavRepository->findById($id);
        if ($Entity) {
            if ($json) {
                echo json_encode($Entity);
            } else {
                return $Entity;
            }
        } else {
            \QException\Exceptions::showHttpStatus(404, 'Nav ' . $id . ' NOT FOUND!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save 
     * -------------------------------------------------------------------------
     */
    public function save(array $data = []) {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;
        $array_positions = filter_input(INPUT_POST, 'array_position',FILTER_DEFAULT,FILTER_REQUIRE_ARRAY);
        $positions = implode(',', $array_positions);
        
        $this->Nav = new NavEntity($data_to_save);
        $this->Nav->set_position($positions);
        $this->NavRepository->store($this->Nav);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id) {
        return $this->NavRepository->remove($id);
    }

}
