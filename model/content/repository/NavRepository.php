<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\repository;
use Qerapp\qcontent\model\content\interfaces\NavMapperInterface,
Qerapp\qcontent\model\content\interfaces\NavRepositoryInterface,
Qerapp\qcontent\model\content\interfaces\NavInterface;
/*
  |*****************************************************************************
  | NavRepositoryRepository
  |*****************************************************************************
  |
  | Repository NavRepository
  | @author qDevTools,
  | @date 2020-03-07 07:49:31,
  |*****************************************************************************
 */

class NavRepository implements NavRepositoryInterface
{
    
    private
            $_NavMapper;
            
    public function __construct(NavMapperInterface $Mapper)
    {
        
         $this->_NavMapper = $Mapper;
        
    }
    
    
      /**
     * -------------------------------------------------------------------------
     * Get all NavRepository
     * -------------------------------------------------------------------------
     * @return NavRepositoryEntity collection
     */
    
    public function findById(int $id)
    {
        return $this->_NavMapper->findOne(['id_nav'=>$id]);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Get all NavRepository
     * -------------------------------------------------------------------------
     * @return NavRepositoryEntity collection
     */
    
    public function findAll(array $conditions = [],array $options = [])
    {
        return $this->_NavMapper->findAll($conditions,$options);
    }
    
    
    /** 
* ------------------------------------------------------------------------- 
* Fin by  id_nav
* ------------------------------------------------------------------------- 
* @param id_nav 
*/ 
  public function findById_nav(int  $id_nav,array $options = [])
{ 
return $this->_NavMapper->findAll(['id_nav'=> $id_nav],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  parent_nav
* ------------------------------------------------------------------------- 
* @param parent_nav 
*/ 
  public function findByParent_nav(int  $parent_nav,array $options = [])
{ 
return $this->_NavMapper->findAll(['parent_nav'=> $parent_nav],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  target
* ------------------------------------------------------------------------- 
* @param target 
*/ 
  public function findByTarget(string $target,array $options = [])
{ 
return $this->_NavMapper->findAll(['target'=> $target],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  url
* ------------------------------------------------------------------------- 
* @param url 
*/ 
  public function findByUrl(string $url,array $options = [])
{ 
return $this->_NavMapper->findAll(['url'=> $url],$options);
}/** 
* ------------------------------------------------------------------------- 
* Fin by  position
* ------------------------------------------------------------------------- 
* @param position 
*/ 
  public function findByPosition(string $position,array $options = [])
{ 
return $this->_NavMapper->findAll(['position'=> $position],$options);
}
    
   
    
     /**
     * -------------------------------------------------------------------------
     * Save NavRepository
     * -------------------------------------------------------------------------
     * @object $NavRepositoryEntity
     * @return type
     */
    public function store(NavInterface $NavRepositoryEntity)
    {
        return $this->_NavMapper->save($NavRepositoryEntity);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Delete NavRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id)
    {
        return $this->_NavMapper->delete($id);
    }
 
}