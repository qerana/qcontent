<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\repository;

use Qerapp\qcontent\model\content\interfaces\NotificationMapperInterface,
    Qerapp\qcontent\model\content\interfaces\NotificationRepositoryInterface,
    Qerapp\qcontent\model\content\interfaces\NotificationInterface;

/*
  |*****************************************************************************
  | NotificationRepositoryRepository
  |*****************************************************************************
  |
  | Repository NotificationRepository
  | @author qDevTools,
  | @date 2020-04-20 08:19:15,
  |*****************************************************************************
 */

class NotificationRepository implements NotificationRepositoryInterface {

    private
            $_NotificationMapper;

    public function __construct(NotificationMapperInterface $Mapper) {

        $this->_NotificationMapper = $Mapper;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all NotificationRepository
     * -------------------------------------------------------------------------
     * @return NotificationRepositoryEntity collection
     */
    public function findById(int $id) {
        return $this->_NotificationMapper->findOne(['id_notification' => $id]);
    }

    public function findNotificacionForPage($conditions) {
     
        return  $this->_NotificationMapper->findOne($conditions);
        
        
    }

    /**
     * -------------------------------------------------------------------------
     * Get all NotificationRepository
     * -------------------------------------------------------------------------
     * @return NotificationRepositoryEntity collection
     */
    public function findAll(array $conditions = [], array $options = []) {
        return $this->_NotificationMapper->findAll($conditions, $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_notification
     * ------------------------------------------------------------------------- 
     * @param id_notification 
     */
    public function findById_notification(int $id_notification, array $options = []) {
        return $this->_NotificationMapper->findAll(['id_notification' => $id_notification], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  title_notification
     * ------------------------------------------------------------------------- 
     * @param title_notification 
     */

    public function findByTitle_notification(string $title_notification, array $options = []) {
        return $this->_NotificationMapper->findAll(['title_notification' => $title_notification], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  content_notification
     * ------------------------------------------------------------------------- 
     * @param content_notification 
     */

    public function findByContent_notification(string $content_notification, array $options = []) {
        return $this->_NotificationMapper->findAll(['content_notification' => $content_notification], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  expiration_date
     * ------------------------------------------------------------------------- 
     * @param expiration_date 
     */

    public function findByExpiration_date(string $expiration_date, array $options = []) {
        return $this->_NotificationMapper->findAll(['expiration_date' => $expiration_date], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Save NotificationRepository
     * -------------------------------------------------------------------------
     * @object $NotificationRepositoryEntity
     * @return type
     */
    public function store(NotificationInterface $NotificationRepositoryEntity) {
        return $this->_NotificationMapper->save($NotificationRepositoryEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete NotificationRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id) {
        return $this->_NotificationMapper->delete($id);
    }

}
