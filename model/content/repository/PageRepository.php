<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\repository;

use Qerapp\qcontent\model\content\interfaces\PageMapperInterface,
    Qerapp\qcontent\model\content\interfaces\PageRepositoryInterface,
    Qerapp\qcontent\model\content\interfaces\PageInterface;

/*
  |*****************************************************************************
  | PageRepositoryRepository
  |*****************************************************************************
  |
  | Repository PageRepository
  | @author qDevTools,
  | @date 2020-03-11 06:31:23,
  |*****************************************************************************
 */

class PageRepository implements PageRepositoryInterface {

    private
            $_PageMapper;

    public function __construct(PageMapperInterface $Mapper) {

        $this->_PageMapper = $Mapper;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all PageRepository
     * -------------------------------------------------------------------------
     * @return PageRepositoryEntity collection
     */
    public function findById(int $id) {
        return $this->_PageMapper->findOne(['id_page' => $id]);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all PageRepository
     * -------------------------------------------------------------------------
     * @return PageRepositoryEntity collection
     */
    public function findAll(array $conditions = [], array $options = []) {
        return $this->_PageMapper->findAll($conditions, $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  id_page
     * ------------------------------------------------------------------------- 
     * @param id_page 
     */
    public function findById_page(int $id_page, array $options = []) {
        return $this->_PageMapper->findAll(['id_page' => $id_page], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  title
     * ------------------------------------------------------------------------- 
     * @param title 
     */

    public function findByTitle(string $title, array $options = []) {
        return $this->_PageMapper->findAll(['title' => $title], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Find Page childs
     * ------------------------------------------------------------------------- 
     * @param title 
     */
    public function findChilds(int $id_page, array $options = []) {
        return $this->_PageMapper->findAll(['parent_page' => $id_page], $options);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Fin by  type
     * ------------------------------------------------------------------------- 
     * @param type 
     */

    public function findByType(string $type, array $options = []) {
        return $this->_PageMapper->findAll(['type' => $type], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  content
     * ------------------------------------------------------------------------- 
     * @param content 
     */

    public function findByContent(string $content, array $options = []) {
        return $this->_PageMapper->findAll(['html_content' => $content], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  meta
     * ------------------------------------------------------------------------- 
     * @param meta 
     */

    public function findByMeta(string $meta, array $options = []) {
        return $this->_PageMapper->findAll(['meta' => $meta], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  keywords
     * ------------------------------------------------------------------------- 
     * @param keywords 
     */

    public function findByKeywords(string $keywords, array $options = []) {
        return $this->_PageMapper->findAll(['keywords' => $keywords], $options);
    }

/**
     * ------------------------------------------------------------------------- 
     * Fin by  layout
     * ------------------------------------------------------------------------- 
     * @param layout 
     */

    public function findByLayout(string $layout, array $options = []) {
        return $this->_PageMapper->findAll(['layout' => $layout], $options);
    }

    /**
     * -------------------------------------------------------------------------
     * Save PageRepository
     * -------------------------------------------------------------------------
     * @object $PageRepositoryEntity
     * @return type
     */
    public function store(PageInterface $PageRepositoryEntity) {
        return $this->_PageMapper->save($PageRepositoryEntity);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete PageRepository
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function remove($id) {
        return $this->_PageMapper->delete($id);
    }

}
