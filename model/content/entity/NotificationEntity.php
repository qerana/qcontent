<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\entity;

use \Ada\EntityManager,
    Qerapp\qcontent\model\content\interfaces\NotificationInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Notification
  |*****************************************************************************
  |
  | Entity Notification
  | @author qDevTools,
  | @date 2020-04-20 08:19:15,
  |*****************************************************************************
 */

class NotificationEntity extends EntityManager implements NotificationInterface, \JsonSerializable {

    protected

    //ATRIBUTES        

/** @var int(11), $id_page  */ 
$_id_page,
    /** @var int(11), $id_notification  */
            $_id_notification,
            /** @var varchar(200), $title_notification  */
            $_title_notification,
            /** @var text(), $content_notification  */
            $_content_notification,
            /** @var date(), $expiration_date  */
            $_expiration_date;

    //RELATED

    public function __construct(array $data = []) {
        $this->populate($data);

        //RELATED-LOAD
    }

//SETTERS

/** 
* ------------------------------------------------------------------------- 
* Setter for id_page
* ------------------------------------------------------------------------- 
* @param int  
*/ 
  public function set_id_page($id_page = 0):void
{ 
$this->_id_page = filter_var($id_page,FILTER_SANITIZE_STRING);
}
    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_notification
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_notification($id_notification = 0): void {
        $this->_id_notification = filter_var($id_notification, FILTER_SANITIZE_STRING);
    }

/**
     * ------------------------------------------------------------------------- 
     * Setter for title_notification
     * ------------------------------------------------------------------------- 
     * @param string 
     */

    public function set_title_notification($title_notification = ""): void {
        $this->_title_notification = filter_var($title_notification, FILTER_SANITIZE_STRING);
    }

/**
     * ------------------------------------------------------------------------- 
     * Setter for content_notification
     * ------------------------------------------------------------------------- 
     * @param string 
     */

    public function set_content_notification($content_notification = ""): void {
        $this->_content_notification = filter_var($content_notification, FILTER_SANITIZE_STRING);
    }

/**
     * ------------------------------------------------------------------------- 
     * Setter for expiration_date
     * ------------------------------------------------------------------------- 
     * @param string 
     */

    public function set_expiration_date($expiration_date = ""): void {
        try {
            $this->_expiration_date = \helpers\Date::toDate($expiration_date);
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException("DateError", $ex);
        }
    }

//GETTERS

/** 
* ------------------------------------------------------------------------- 
* Getter for id_page
* ------------------------------------------------------------------------- 
* @return int   
*/ 
  public function get_id_page()
{ 
 return  $this->_id_page;
}

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_notification
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_notification() {
        return $this->_id_notification;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for title_notification
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_title_notification() {
        return filter_var($this->_title_notification, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for content_notification
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_content_notification() {
        return filter_var($this->_content_notification, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for expiration_date
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_expiration_date() {
        return \helpers\Date::toString($this->_expiration_date);
    }

}
