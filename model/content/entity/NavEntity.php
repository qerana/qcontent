<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\entity;

use \Ada\EntityManager,
    Qerapp\qcontent\model\content\interfaces\NavInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Nav
  |*****************************************************************************
  |
  | Entity Nav
  | @author qDevTools,
  | @date 2020-03-07 07:49:31,
  |*****************************************************************************
 */

class NavEntity extends EntityManager implements NavInterface, \JsonSerializable {

    protected

    //ATRIBUTES        

    /** @var varchar(200), $navname  */
            $_navname,
            /** @var int(11), $id_nav  */
            $_id_nav,
            /** @var int(11), $parent_nav  */
            $_parent_nav,
            /** @var varchar(45), $target  */
            $_target,
            /** @var varchar(150), $url  */
            $_url,
            /** @var varchar(100), $position  */
            $_position;
    //RELATED
    public
            $Childs;

    public function __construct(array $data = []) {
        $this->populate($data);

        //RELATED-LOAD
    }

//SETTERS

    /**
     * ------------------------------------------------------------------------- 
     * Setter for navname
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_navname($navname = ""): void {
        $this->_navname = filter_var($navname, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_nav
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_nav($id_nav = 0): void {
        $this->_id_nav = filter_var($id_nav, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for parent_nav
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_parent_nav($parent_nav = 0): void {
        $this->_parent_nav = filter_var($parent_nav, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for target
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_target($target = ""): void {
        $this->_target = filter_var($target, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for url
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_url($url = ""): void {
        $this->_url = filter_var($url, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for position
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_position($position = ""): void {
        $this->_position = filter_var($position, FILTER_SANITIZE_STRING);
    }

//GETTERS

    /**
     *  Convert each position to array element
     * @return type
     */
    public function getPositions() {
        return explode(',', $this->_position);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for navname
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_navname() {
        return filter_var($this->_navname, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_nav
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_nav() {
        return $this->_id_nav;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for parent_nav
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_parent_nav() {
        return $this->_parent_nav;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for target
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_target() {
        return filter_var($this->_target, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for url
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_url() {
        return filter_var($this->_url, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for position
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_position() {
        return filter_var($this->_position, FILTER_SANITIZE_STRING);
    }

}
