<?php

/*
 * Copyright (C) 2019/20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\entity;

use \Ada\EntityManager,
    Qerapp\qcontent\model\content\interfaces\PageInterface;

//RELATED-NS


/*
  |*****************************************************************************
  | ENTITY CLASS for Page
  |*****************************************************************************
  |
  | Entity Page
  | @author qDevTools,
  | @date 2020-03-11 06:31:23,
  |*****************************************************************************
 */

class PageEntity extends EntityManager implements PageInterface, \JsonSerializable {

    public
            $Notification;
    protected

//ATRIBUTES        

    /** @var text(), $namespace_destiny  */
            $_namespace_destiny,
            /** @var datetime(), $publication_date  */
            $_publication_date,
            /** @var int(11), $id_page  */
            $_id_page,
            /** @var int $parent page  */
            $_parent_page,
            $_title,
            /** @var varchar(255), $type  */
            $_type,
            /** @var text(), $content  */
            $_html_content,
            /** @var text(), $meta  */
            $_meta,
            /** @var text(), $keywords  */
            $_keywords,
            /** @var string $notification */
            $_notification,
            /** @var string $notification_config */
            $_notification_config,
            /** @var varchar(45), $layout  */
            $_layout;

//RELATED

    public function __construct(array $data = []) {
        $this->populate($data);

//RELATED-LOAD
    }

//SETTERS


    public function setNotification() {

        $Notification = \Ada\EntityRelation::hasOne($this, 'notification', 'id_page', 'findNotificacionForPage');
        if ($Notification->expiration_date >= date('Y-m-d 00:00:00')) {
            $this->Notification = $Notification;
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for namespace_destiny
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_namespace_destiny($namespace_destiny = ""): void {
        $this->_namespace_destiny = $namespace_destiny;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for publication_date
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_publication_date($publication_date = ""): void {
        try {
            $this->_publication_date = \helpers\Date::toDate($publication_date);
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException("DateError", $ex);
        }
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for id_page
     * ------------------------------------------------------------------------- 
     * @param int  
     */
    public function set_id_page($id_page = 0): void {
        $this->_id_page = filter_var($id_page, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for title
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_title($title = ""): void {
        $this->_title = filter_var($title, FILTER_SANITIZE_STRING);
    }

    function set_parent_page($_parent_page = 0) {
        $this->_parent_page = filter_var($_parent_page, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for type
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_type($type = ""): void {
        $this->_type = filter_var($type, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for content
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_html_content($content = ""): void {
        $this->_html_content = htmlspecialchars_decode($content);
//$this->_html_content = $content;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for meta
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_meta($meta = ""): void {
        $this->_meta = filter_var($meta, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for keywords
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_keywords($keywords = ""): void {
        $this->_keywords = filter_var($keywords, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Setter for layout
     * ------------------------------------------------------------------------- 
     * @param string 
     */
    public function set_layout($layout = ""): void {
        $this->_layout = filter_var($layout, FILTER_SANITIZE_STRING);
    }

    function set_notification(string $_notification = ''): void {
        $this->_notification = htmlspecialchars_decode($_notification);
    }

    function set_notification_config(string $_notification_config = ''): void {
        $this->_notification_config = $_notification_config;
    }

//GETTERS

    /**
     * Get type equals
     * @return string
     */
    public function getType() {
        switch ($this->_type) {
            case 1:
                $type = 'SimpleContent';
                break;
            case 2;
                $type = 'ComplexContent';
                break;
        }

        return $type;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for namespace_destiny
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_namespace_destiny() {
        return filter_var($this->_namespace_destiny, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for publication_date
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_publication_date() {
        return \helpers\Date::toString($this->_publication_date, "d-m-Y H:i:s");
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for id_page
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_id_page() {
        return $this->_id_page;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for parent_page
     * ------------------------------------------------------------------------- 
     * @return int   
     */
    public function get_parent_page() {
        return $this->_parent_page;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for title
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_title() {
        return filter_var($this->_title, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for type
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_type() {
        return filter_var($this->_type, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for content
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_content() {
        return $this->_html_content;
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for meta
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_meta() {
        return filter_var($this->_meta, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for keywords
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_keywords() {
        return filter_var($this->_keywords, FILTER_SANITIZE_STRING);
    }

    /**
     * ------------------------------------------------------------------------- 
     * Getter for layout
     * ------------------------------------------------------------------------- 
     * @return string  
     */
    public function get_layout() {
        return filter_var($this->_layout, FILTER_SANITIZE_STRING);
    }

    function get_notification() {
        return $this->_notification;
    }

    function get_notification_config() {
        return $this->_notification_config;
    }

}
