<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content;

use Qerapp\qcontent\model\content\entity\NotificationEntity,
    Qerapp\qcontent\model\content\mapper\NotificationMapper,
    Qerapp\qcontent\model\content\interfaces\NotificationMapperInterface,
    Qerapp\qcontent\model\content\repository\NotificationRepository;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity NotificationService
  | @author qDevTools,
  | @date 2020-04-20 08:19:15,
  |*****************************************************************************
 */

class NotificationService {

    public
    //RELATED-MAPPER-OBJECT
            $NotificationRepository,
            /** @object entity Notification */
            $Notification;

    public function __construct(NotificationMapperInterface $Mapper = null) {

        //RELATED-MAPPER-OBJECT-NEW



        try {
            $NotificationMapper = new NotificationMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.NotificationService', $ex);
        }

        $MapperRepository = (is_null($Mapper)) ? $NotificationMapper : $Mapper;
        $this->NotificationRepository = new NotificationRepository($MapperRepository);
    }

    /**
     * -------------------------------------------------------------------------
     * simple test
     * -------------------------------------------------------------------------
     */
    public function runTest() {


        echo '<h1>Test</h1>';
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false) {

        $Collection = $this->NotificationRepository->findAll();
        if ($json) {
            echo json_encode($Collection);
        } else {
            return $Collection;
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false) {

        $Entity = $this->NotificationRepository->findById($id);
        if ($Entity) {
            if ($json) {
                echo json_encode($Entity);
            } else {
                return $Entity;
            }
        } else {
            \QException\Exceptions::showHttpStatus(404, 'Notification ' . $id . ' NOT FOUND!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save 
     * -------------------------------------------------------------------------
     */
    public function save(array $data = []) {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData() : $data;

        $this->Notification = new NotificationEntity($data_to_save);
        $this->NotificationRepository->store($this->Notification);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id) {
        return $this->NotificationRepository->remove($id);
    }

}
