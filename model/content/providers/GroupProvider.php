<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\providers;

use Qerapp\qcontent\model\content\interfaces\PageInterface,
    Qerapp\qcontent\model\content\mapper\PageMapper,
    Qerapp\qcontent\model\content\repository\PageRepository;

class GroupProvider {

    public
            $Page,
            $Repository;

    public function __construct(PageInterface $Page) {
        $this->Page = $Page;
        $this->setRepository();
        $this->prepareGroupPage();
    }

    /**
     *  Set page repository
     */
    private function setRepository() {

        try {
            $PageMapper = new PageMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.PageService.GroupProvider', $ex);
        }
        $this->Repository = new PageRepository($PageMapper);
    }
    
    private function getChilds(){
      
        return $this->Repository->findChilds($this->Page->id_page);
    }

    /**
     *  Prepare content for page
     * @throws \Exception
     */
    private function prepareGroupPage() {
        try {

            $childs = $this->getChilds();
            
            $content = $this->Page->html_content;
            $new_content =  \Qerana\core\View::showView('page/page_groups/groups1', ['childs' => $childs], true);
            
            $this->Page->html_content =   $content.$new_content;
            
        } catch (\Throwable $ex) {

            \QException\Exceptions::ShowException('CriticalError', $ex);
        }
    }

}
