<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\providers;

use Qerapp\qcontent\model\content\interfaces\PageInterface;

class ComplexProvider {

    public
            $Page;

    public function __construct(PageInterface $Page) {
        $this->Page = $Page;
        $this->preparePluginPage();
    }

    /**
     *  Prepare content for page
     * @throws \Exception
     */
    private function preparePluginPage() {
        try {

            // if type==2, then try to run a service in plugins folder
            $ns_parts = explode('/', $this->Page->namespace_destiny);
            $ClassName = '\\Qerapp\\qcontent\\plugins\\' . $ns_parts[0] . '\\' . $ns_parts[1];
            $MethodName = $ns_parts[2];


            if (is_callable([$ClassName, $MethodName]) == false) {
                throw new \Exception('Qcontent.PageService', $ClassName . ':' . $MethodName . ', cant call!!');
            }
            $Service = new $ClassName;
            $content = $Service->$MethodName();
            $this->Page->html_content = $content;
        } catch (\Throwable $ex) {

            \QException\Exceptions::ShowException('CriticalError', $ex);
        }
    }

}
