<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content;
use Qerapp\qcontent\model\content\NavService;
require_once(__QERAPPSFOLDER__ . 'qcontent/config/_config.php');

class QContentService {

    private static
    /** @boolean is_ajax request? */
            $_is_ajax;

    /**
     * -------------------------------------------------------------------------
     * Show a view template
     * -------------------------------------------------------------------------
     * @param string $template , html template
     * @param array $vars , parameters to pass to a view
     * @param boolean $save , if you want to sotre the rendered view in a variable
     */
    public static function show($vars = '', bool $save = false) {

        self::_checkAjaxPetition();

        // plugins to load
        $plugins = (isset($vars['Plugins'])) ? $vars['Plugins'] : [];

        $template_path = 'qcontent/view/page/view_page.php';
        $full_path_template = __QERAPPSFOLDER__ . $template_path;

        // check if template path exists
        // if not exists, pass the template to a viewMaker to try to create it


        (!file_exists($full_path_template)) ?
                        \QException\Exceptions::showError('View error',
                                'View dont exists, or is misspelled <b>' . $template_path) . '</b>' : '';

        // processs the parameters is available in the template view
        if (is_array($vars)) {
            foreach ($vars as $key => $valor) {
                $$key = $valor;
            }
        }

        // if $save is true, create a buffer and store tne entire template rendered
        // in a variable, & return this variable
        if ($save) {
            ob_start();
            include($full_path_template );
            $var_view = ob_get_contents();
            ob_end_clean();
            return $var_view;
        }

        // check if layout exists

        if (!realpath(__QTEMPLATE__)) {
            \QException\Exceptions::showError('ViewError', 'The layout dont exists');
        }


        // if want to load the html header
        if(!self::$_is_ajax) { 
            // parse nav
            
            $NavService = new NavService();
            $Navs = $NavService->getNavMenu();
            require_once(__QTEMPLATE__ . '/_html_header.php');
            
        }

        // include the template file
        include($full_path_template);

        // if want to load the footer


        $footer_path = realpath(__QTEMPLATE__ . '/_html_footer.php');



        (!self::$_is_ajax) ? require_once($footer_path) : require_once(__ROOTFOLDER__ . '/_layouts_/_plugins.php');
    }

    /**
     * -------------------------------------------------------------------------
     * Check if is a jax petition
     * -------------------------------------------------------------------------
     */
    private static function _checkAjaxPetition() {

        $server_request = FILTER_INPUT(INPUT_SERVER, "HTTP_X_REQUESTED_WITH");
        self::$_is_ajax = (isset($server_request) AND $server_request === 'XMLHttpRequest') ? true : false;
    }
    
    

}
