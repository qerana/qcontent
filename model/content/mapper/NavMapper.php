<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\mapper;

use Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\adapters\XmlAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qcontent\model\content\entity\NavEntity,
    Qerapp\qcontent\model\content\interfaces\NavMapperInterface,
    Qerapp\qcontent\model\content\interfaces\NavInterface;

//RELATED-NAMESPACES    

/*
  |*****************************************************************************
  | MAPPER CLASS for NavMapperEntity
  |*****************************************************************************
  |
  | MAPPER NavMapper
  | @author qDevTools,
  | @date 2020-03-07 07:49:31,
  |*****************************************************************************
 */

class NavMapper extends AdaDataMapper implements NavMapperInterface {

    //RELATED-MAPPERS

    public function __construct(AdapterInterface $Adapter = null) {

        $navs_db = __DATA__ . 'xml/qcontent/navs';
        $this->_Adapter = (is_null($Adapter) ) ? new XmlAdapter($navs_db, 'nav', false) : $Adapter;

        //RELATED-MAPPER-OBJECT


        parent::__construct($this->_Adapter);
    }

    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */

    public function findById(int $id) {
        $row = $this->_Adapter->find(['id_nav' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(NavInterface $Nav) {

        $data = parent::getDataObject($Nav);

        if (is_null($Nav->id_nav)) {
            $Nav->id_nav = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_nav' => $Nav->id_nav]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($Nav) {

        // if $id is a object and a UserEntity  
        if ($Nav instanceof NavInterface) {
            $Nav = $Nav->id_nav;
        }

        return $this->_Adapter->delete(['id_nav' => $Nav]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): NavEntity {
        $NavEntity = new NavEntity($row);
        return $NavEntity;
    }

}
