<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\mapper;

use Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\adapters\XmlAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qcontent\model\content\entity\NotificationEntity,
    Qerapp\qcontent\model\content\interfaces\NotificationMapperInterface,
    Qerapp\qcontent\model\content\interfaces\NotificationInterface;

//RELATED-NAMESPACES    

/*
  |*****************************************************************************
  | MAPPER CLASS for NotificationMapperEntity
  |*****************************************************************************
  |
  | MAPPER NotificationMapper
  | @author qDevTools,
  | @date 2020-04-20 08:19:15,
  |*****************************************************************************
 */

class NotificationMapper extends AdaDataMapper implements NotificationMapperInterface {

    //RELATED-MAPPERS

    public function __construct(AdapterInterface $Adapter = null) {

        $notifications_db = __DATA__.'xml/qcontent/notifications';
        $this->_Adapter = (is_null($Adapter) ) ? new XmlAdapter($notifications_db, 'notification',false) : $Adapter;

        //RELATED-MAPPER-OBJECT


        parent::__construct($this->_Adapter);
    }

    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */

    public function findById(int $id) {
        $row = $this->_Adapter->find(['id_notification' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(NotificationInterface $Notification) {

        $data = parent::getDataObject($Notification);

        if (is_null($Notification->id_notification)) {
            $Notification->id_notification = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_notification' => $Notification->id_notification]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($Notification) {

        // if $id is a object and a UserEntity  
        if ($Notification instanceof NotificationInterface) {
            $Notification = $Notification->id_notification;
        }

        return $this->_Adapter->delete(['id_notification' => $Notification]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): NotificationEntity {
        $NotificationEntity = new NotificationEntity($row);
        return $NotificationEntity;
    }

}
