<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\mapper;

use Ada\adapters\AdapterInterface,
    Ada\adapters\PDOAdapter,
    Ada\adapters\XmlAdapter,
    Ada\mapper\AdaDataMapper,
    Qerapp\qcontent\model\content\entity\PageEntity,
    Qerapp\qcontent\model\content\interfaces\PageMapperInterface,
    Qerapp\qcontent\model\content\interfaces\PageInterface;

//RELATED-NAMESPACES    

/*
  |*****************************************************************************
  | MAPPER CLASS for PageMapperEntity
  |*****************************************************************************
  |
  | MAPPER PageMapper
  | @author qDevTools,
  | @date 2020-03-11 06:31:23,
  |*****************************************************************************
 */

class PageMapper extends AdaDataMapper implements PageMapperInterface {

    //RELATED-MAPPERS

    public function __construct(AdapterInterface $Adapter = null) {

        $pages_db = __DATA__.'xml/qcontent/pages';
        $this->_Adapter = (is_null($Adapter) ) ? new XmlAdapter($pages_db, 'page',false) : $Adapter;

        //RELATED-MAPPER-OBJECT


        parent::__construct($this->_Adapter);
    }

    /*
     * -------------------------------------------------------------------------
     * Find by id
     * -------------------------------------------------------------------------
     * @param int $id
     * @return EntityObject
     */

    public function findById(int $id) {
        $row = $this->_Adapter->find(['id_page' => $id], ['fetch' => 'one']);

        // if row exists , the create a new entity, otherwise null is returned
        if (!$row) {
            return null;
        } else {
            return $this->createEntity($row);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save entity
     * -------------------------------------------------------------------------
     */
    public function save(PageInterface $Page) {

        $data = parent::getDataObject($Page,false);//false parameter to allow empty attr
        
        if (is_null($Page->id_page)) {
            $Page->id_page = $this->_Adapter->insert($data);
        } else {
            $this->_Adapter->update($data, ['id_page' => $Page->id_page]);
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Delete entity
     * -------------------------------------------------------------------------
     * @param mixed $id
     * @return type
     */
    public function delete($Page) {

        // if $id is a object and a UserEntity  
        if ($Page instanceof PageInterface) {
            $Page = $Page->id_page;
        }

        return $this->_Adapter->delete(['id_page' => $Page]);
    }

    /**
     * -------------------------------------------------------------------------
     * Create Entity
     * -------------------------------------------------------------------------
     * @param array $row
     * @return Entity
     */
    protected function createEntity(array $row): PageEntity {
        $PageEntity = new PageEntity($row);
        return $PageEntity;
    }

}
