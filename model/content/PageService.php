<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content;

use Qerapp\qcontent\model\content\entity\PageEntity,
    Qerapp\qcontent\model\content\mapper\PageMapper,
    Qerapp\qcontent\model\content\interfaces\PageMapperInterface,
    Qerapp\qcontent\model\content\repository\PageRepository;

//RELATED-MAPPERS

/*
  |*****************************************************************************
  | [{server_name}]
  |*****************************************************************************
  |
  | Service for Entity PageService
  | @author qDevTools,
  | @date 2020-03-11 06:31:23,
  |*****************************************************************************
 */

class PageService {

    public
    //RELATED-MAPPER-OBJECT
            $PageRepository,
            $page_types = ['simple', 'complex', 'group'],
            /** @object entity Page */
            $Page;

    public function __construct(PageMapperInterface $Mapper = null) {

        //RELATED-MAPPER-OBJECT-NEW
        try {
            $PageMapper = new PageMapper;
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Mapper.PageService', $ex);
        }

        $MapperRepository = (is_null($Mapper)) ? $PageMapper : $Mapper;
        $this->PageRepository = new PageRepository($MapperRepository);
    }

    /**
     * -------------------------------------------------------------------------
     * Get all
     * -------------------------------------------------------------------------
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getAll($json = false) {

        $Collection = $this->PageRepository->findAll();
        if ($json) {
            echo json_encode($Collection);
        } else {
            return $Collection;
        }
    }

    /**
     * get by title
     * @param string $title
     * @return type
     */
    public function getByTitle(string $title) {

        $title_search = substr($title, 0, -5);
        // remove white space
        $clean_title = str_replace('%20', " ", $title_search);

        $Page = $this->PageRepository->findByTitle($clean_title, ['fetch' => 'one']);

        if (!$Page) {
            \QException\Exceptions::showHttpStatus('404', 'Sorry, this content"' . $title . '" not longer exists!!.');
        }
        $Page->setNotification();
        $this->Page = $Page;
        
    }

    /**
     * get page
     */
    public function getPage(string $title) {

        $this->getByTitle($title);

        if ($this->Page->type !== 'simple') {
            
            $class_provider = 'Qerapp\\qcontent\\model\\content\\providers\\'. ucfirst($this->Page->type).'Provider';
            $Provider = new $class_provider($this->Page);
            $this->Page = $Provider->Page;
           
        }

        return $this->Page;
    }

    /**
     * If PAge type Equal 2, then process internal qcontent plugin
     * @return type
     * @throws \Exception
     */
    

    /**
     * -------------------------------------------------------------------------
     * Get by id
     * -------------------------------------------------------------------------
     * @param $id , id entity
     * @param $json , true return data in json format, otherwise object collection
     */
    public function getById(int $id, $json = false) {

        $Entity = $this->PageRepository->findById($id);
        if ($Entity) {
            if ($json) {
                echo json_encode($Entity);
            } else {
                return $Entity;
            }
        } else {
            \QException\Exceptions::showHttpStatus(404, 'Page ' . $id . ' NOT FOUND!!');
        }
    }

    /**
     * -------------------------------------------------------------------------
     * Save 
     * -------------------------------------------------------------------------
     */
    public function save(array $data = []) {

        $data_to_save = (empty($data)) ? \helpers\Request::getFormData(false) : $data;
        
        $this->Page = new PageEntity($data_to_save);
        $this->PageRepository->store($this->Page);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return type
     */
    public function delete(int $id) {
        return $this->PageRepository->remove($id);
    }

}
