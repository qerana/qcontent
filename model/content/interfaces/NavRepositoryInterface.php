<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\interfaces;
use  Qerapp\qcontent\model\content\interfaces\NavInterface;
/*
  |*****************************************************************************
  | [{model_name}]RepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE [{model_name}]
  | @author qDevTools,
  | @date 2020-03-07 07:49:31,
  |*****************************************************************************
 */

interface NavRepositoryInterface 
{
    
    public function findAll(array $conditions = [],array $options = []);
    
     public function findById_nav(int  $id_nav,array $options = []);
 public function findByParent_nav(int  $parent_nav,array $options = []);
 public function findByTarget(string $target,array $options = []);
 public function findByUrl(string $url,array $options = []);
 public function findByPosition(string $position,array $options = []);

    
    public function store(NavInterface $User);
    
    public function remove($id);
 
 
}