<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\interfaces;
/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-04-20 08:19:15,
  |*****************************************************************************
 */

interface NotificationInterface
{

    
//SETTERS

  public function set_id_notification(int  $id_notification):void; 
 public function set_title_notification(string $title_notification):void; 
 public function set_content_notification(string $content_notification):void; 
 public function set_expiration_date(string $expiration_date):void; 

    
//GETTERS

  public function get_id_notification();
 public function get_title_notification();
 public function get_content_notification();
 public function get_expiration_date();

 
 
 
}