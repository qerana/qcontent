<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\interfaces;
use  Qerapp\qcontent\model\content\interfaces\NotificationInterface;
/*
  |*****************************************************************************
  | [{model_name}]RepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE [{model_name}]
  | @author qDevTools,
  | @date 2020-04-20 08:19:15,
  |*****************************************************************************
 */

interface NotificationRepositoryInterface 
{
    
    public function findAll(array $conditions = [],array $options = []);
    
     public function findById_notification(int  $id_notification,array $options = []);
 public function findByTitle_notification(string $title_notification,array $options = []);
 public function findByContent_notification(string $content_notification,array $options = []);
 public function findByExpiration_date(string $expiration_date,array $options = []);

    
    public function store(NotificationInterface $User);
    
    public function remove($id);
 
 
}