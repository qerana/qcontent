<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\interfaces;

/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-03-11 06:31:23,
  |*****************************************************************************
 */

interface PageInterface {

//SETTERS

    public function set_id_page(int $id_page): void;

    public function set_title(string $title): void;

    public function set_type(string $type): void;

    public function set_html_content(string $content): void;

    public function set_meta(string $meta): void;

    public function set_keywords(string $keywords): void;

    public function set_layout(string $layout): void;

//GETTERS

    public function get_id_page();

    public function get_title();

    public function get_type();

    public function get_content();

    public function get_meta();

    public function get_keywords();

    public function get_layout();
}
