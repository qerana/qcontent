<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\interfaces;
use  Qerapp\qcontent\model\content\interfaces\PageInterface;
/*
  |*****************************************************************************
  | [{model_name}]RepositoryInterface
  |*****************************************************************************
  |
  | Repository INTERFACE [{model_name}]
  | @author qDevTools,
  | @date 2020-03-11 06:31:23,
  |*****************************************************************************
 */

interface PageRepositoryInterface 
{
    
    public function findAll(array $conditions = [],array $options = []);
    
     public function findById_page(int  $id_page,array $options = []);
 public function findByTitle(string $title,array $options = []);
 public function findByType(string $type,array $options = []);
 public function findByContent(string $content,array $options = []);
 public function findByMeta(string $meta,array $options = []);
 public function findByKeywords(string $keywords,array $options = []);
 public function findByLayout(string $layout,array $options = []);

    
    public function store(PageInterface $User);
    
    public function remove($id);
 
 
}