<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\model\content\interfaces;

/*
  |*****************************************************************************
  | INTERFACE  for [{folder_name}]
  |*****************************************************************************
  |
  | Entity [{folder_name}]
  | @author qDevTools,
  | @date 2020-03-07 07:49:31,
  |*****************************************************************************
 */

interface NavInterface {

//SETTERS

    public function set_id_nav(int $id_nav): void;

    public function set_parent_nav(int $parent_nav): void;

    public function set_target(string $target): void;

    public function set_url(string $url): void;

    public function set_position(string $position): void;

//GETTERS

    public function get_id_nav();

    public function get_parent_nav();

    public function get_target();

    public function get_url();

    public function get_position();
}
