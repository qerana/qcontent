<nav class="colorlib-nav" role="navigation">
    <div class="top-menu bg-primary">
        <div class="container">
            <div class="row">
                <div class="col-md-2">
                    <div id="colorlib-logo"><a href="<?php echo __URL__;?>"><?php echo __APPNAME__; ?></a></div>
                </div>
                <div class="col-md-10 text-right menu-1">
                    <ul>
                        <?php
                        foreach ($Navs AS $Nav):

                            // if not has childs
                            if (empty($Nav->Childs)) {
                                ?>
                                <li class="active">
                                    <a href="<?php echo $Nav->url; ?>" 
                                       target="<?php echo $Nav->target;?>">
                                           <?php echo $Nav->navname; ?>
                                    </a>
                                </li>

                                <?php
                            }
                            // if has childs
                            else {
                                ?>
                                <li class="has-dropdown">
                                    <a href="work.html"><?php echo $Nav->navname; ?></a>
                                    <ul class="dropdown">
                                        <?php foreach($Nav->Childs AS $Child): ?>
                                        <li><a href="<?php echo $Child->url;?>"><?php echo $Child->navname;?></a></li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>

                                <?php
                            }
                        endforeach;
                        ?>

                    </ul>
                </div>
            </div>
        </div>
    </div>
</nav>
