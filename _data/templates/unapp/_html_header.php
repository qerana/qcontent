<!DOCTYPE HTML>
<html>
	<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo __APPNAME__.'-'.$Page->title; ?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="<?php echo $Page->meta;?>" />
	<meta name="keywords" content="<?php echo $Page->keywords;?>" />
	<meta name="author" content="iprprevencion.es" />


	<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css?family=Nunito:200,300,400" rel="stylesheet">
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="/_styles/qcontent/templates/unapp/css/animate.css">
	<!-- Icomoon Icon Fonts-->
	<link rel="stylesheet" href="/_styles/qcontent/templates/unapp/css/icomoon.css">
	<!-- Bootstrap  -->
	<link rel="stylesheet" href="/_styles/qcontent/templates/unapp/css/bootstrap.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="/_styles/qcontent/templates/unapp/css/magnific-popup.css">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="/_styles/qcontent/templates/unapp/css/owl.carousel.min.css">
	<link rel="stylesheet" href="/_styles/qcontent/templates/unapp/css/owl.theme.default.min.css">

	<!-- Theme style  -->
	<link rel="stylesheet" href="/_styles/qcontent/templates/unapp/css/style.css">

	<!-- Modernizr JS -->
	<script src="/_styles/qcontent/templates/unapp/js/modernizr-2.6.2.min.js"></script>
	<!-- FOR IE9 below -->
	<!--[if lt IE 9]>
	<script src="js/respond.min.js"></script>
	<![endif]-->

	</head>
	<body>
		
	<div class="colorlib-loader"></div>

	<div id="page">
            <?php include '_navtop.php'; 