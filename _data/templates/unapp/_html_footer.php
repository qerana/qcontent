</div>
<div class="gototop js-top">
    <a href="#" class="js-gotop"><i class="icon-arrow-up2"></i></a>
</div>


<!-- Modal -->
<div class="modal fade" id="notification" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"><?php echo $Page->Notification->title_notification; ?></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
              <?php echo $Page->Notification->content_notification; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- jQuery -->
<script src="/_styles/qcontent/templates/unapp/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="/_styles/qcontent/templates/unapp/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="/_styles/qcontent/templates/unapp/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="/_styles/qcontent/templates/unapp/js/jquery.waypoints.min.js"></script>
<!-- Stellar Parallax -->
<script src="/_styles/qcontent/templates/unapp/js/jquery.stellar.min.js"></script>
<!-- YTPlayer -->
<script src="/_styles/qcontent/templates/unapp/js/jquery.mb.YTPlayer.min.js"></script>
<!-- Owl carousel -->
<script src="/_styles/qcontent/templates/unapp/js/owl.carousel.min.js"></script>
<!-- Magnific Popup -->
<script src="/_styles/qcontent/templates/unapp/js/jquery.magnific-popup.min.js"></script>
<script src="/_styles/qcontent/templates/unapp/js/magnific-popup-options.js"></script>
<!-- Counters -->
<script src="/_styles/qcontent/templates/unapp/js/jquery.countTo.js"></script>
<!-- Main -->
<script src="/_styles/qcontent/templates/unapp/js/main.js"></script>

<script>

    notification = '<?php echo $Page->Notification->title_notification; ?>';

    if (notification !== '') {
        $('#notification').modal('show');
    }



</script>

</body>

