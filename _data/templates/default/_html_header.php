<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo __APPNAME__.'-'.$Page->title; ?></title>
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Custom fonts for this template-->
        <link href="https://fonts.googleapis.com/css?family=Crimson+Text:400,400i,600|Montserrat:200,300,400" rel="stylesheet">

        <link rel="stylesheet" href="/_styles/qcontent/templates/landing/assets/css/bootstrap/bootstrap.css">
        <link rel="stylesheet" href="/_styles/qcontent/templates/landing/assets/fonts/ionicons/css/ionicons.min.css">

        <link rel="stylesheet" href="/_styles/qcontent/templates/landing/assets/fonts/fontawesome/css/font-awesome.min.css">


        <link rel="stylesheet" href="/_styles/qcontent/templates/landing/assets/css/slick.css">
        <link rel="stylesheet" href="/_styles/qcontent/templates/landing/assets/css/slick-theme.css">

        <link rel="stylesheet" href="/_styles/qcontent/templates/landing/assets/css/helpers.css">
        <link rel="stylesheet" href="/_styles/qcontent/templates/landing/assets/css/style.css">
        <link rel="stylesheet" href="/_styles/qcontent/templates/landing/assets/css/landing-2.css">
        <script src="/_styles/qcontent/templates/landing/assets/js/jquery.min.js"></script>

    </head>
    <body data-spy="scroll" data-target="#pb-navbar" data-offset="200">
        <?php include '_navtop.php'; ?>