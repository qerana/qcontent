<nav class="navbar navbar-expand-lg navbar-dark pb_navbar pb_scrolled-light" id="pb-navbar">
    <div class="container">
        <a class="navbar-brand" href="">
            <b><?php echo __APPNAME__;?></b>
        </a>
        <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" 
                data-target="#probootstrap-navbar" aria-controls="probootstrap-navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span><i class="ion-navicon"></i></span>
        </button>
        <div class="collapse navbar-collapse" id="probootstrap-navbar">
            <ul class="navbar-nav ml-auto">
                <?php foreach ($Navs AS $Nav): ?>
                    <li class="nav-item ">
                        <a class="nav-link" href="<?php echo $Nav->url; ?>"><?php echo $Nav->navname; ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
        
    </div>
</nav>