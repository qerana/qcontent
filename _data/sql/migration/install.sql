/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  diemarc
 * Created: Mar 11, 2020
 */

CREATE DATABASE IF NOT EXISTS  `qerana_fw` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE  IF NOT EXISTS `qcontent_navs` (
  `id_nav` int(11) NOT NULL AUTO_INCREMENT,
  `navname` varchar(200) DEFAULT NULL,
  `parent_nav` int(11) NOT NULL DEFAULT '0' COMMENT 'child navs supports',
  `target` varchar(45) NOT NULL DEFAULT '_self' COMMENT '_blanl,_parent,_self, etc',
  `url` varchar(150) NOT NULL,
  `position` varchar(100) DEFAULT NULL COMMENT 'top,bottom, left, rigth,  store the posicion of the nav, ie: top;bottom',
  PRIMARY KEY (`id_nav`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



CREATE TABLE  IF NOT EXISTS  `qcontent_pages` (
  `id_page` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL COMMENT '1=standard page, 2=redirect to controller ns',
  `html_content` text,
  `meta` text,
  `keywords` text,
  `layout` varchar(45) DEFAULT NULL,
  `namespace_destiny` text COMMENT 'if type=2, then on load the page , run this namespace, save the view result and show ina dierent view.',
  `publication_date` datetime DEFAULT NULL,
  PRIMARY KEY (`id_page`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



CREATE TABLE  IF NOT EXISTS  `qerana_fw`.`qcontent_notifications` (
  `id_notification` INT NOT NULL AUTO_INCREMENT,
  `title_notification` VARCHAR(200) NULL,
  `content_notification` TEXT NULL,
  `expiration_date` DATE NULL,
  PRIMARY KEY (`id_notification`))
ENGINE = MyISAM;
