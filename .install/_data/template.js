// load all data
function loadTemplate() {
    getDataJson('/qemail/Template/getAllInJson', 'fill()');
}

// fill index table
function fill() {

    var list_Template = '';

    $.each(data_response, function (i, Template) {
        list_Template += '<tr class="small">';
        list_Template += '<td><a href="/qemail/Template/detail/' + Template.id_qemail_tpl + '" class="btn btn-outline-primary btn-sm" title="Detalle Template"><i class="fa fa-tasks"></i></a>';

        list_Template += '<td>' + Template.tpl_name + '</td>';
        list_Template += '<td>' + Template.tpl_type + '</td>';
        list_Template += '<td>' + Template.tpl_active + '</td>';

        list_Template += '</tr>';

    });
    $('#list_Template').html(list_Template);

}

// load data
function loadDataTemplate() {

    var id_qemail_tpl = $('#data_Template').attr("data-Template");
    getDataJson('/qemail/Template/getOneInJson/' + id_qemail_tpl, 'fillDataTemplate()');

}


// fill data detail
function fillDataTemplate() {

    var data_json = ' ';
    var detail_title = '';

    $.each(data_response, function (i, Template) {


        data_json += ' <div class="row m-1">';
        data_json += '    <div class="col-sm-3 bg-gray-100 p-2">';
        data_json += '        <b>' + i + '</b>';
        data_json += '    </div>';
        data_json += '    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_' + i + '">';
        data_json += '        ' + Template + '';
        data_json += '    </div>';
        data_json += '</div>';

    });


    $('#detail_title').html(detail_title);
    $('#data_Template').html(data_json);

}