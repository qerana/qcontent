// load all data
function loadAccount() {
    getDataJson('/qemail/Account/getAllInJson', 'fill()');
}

// fill index table
function fill() {

    var list_Account = '';

    $.each(data_response, function (i, Account) {
        list_Account += '<tr class="small">';
        list_Account += '<td><a href="/qemail/Account/detail/' + Account.id_account + '" class="btn btn-outline-primary btn-sm" title="Detalle Account"><i class="fa fa-tasks"></i></a>';

        list_Account += '<td>' + Account.id_account + '</td>';
        list_Account += '<td>' + Account.account + '</td>';
        list_Account += '<td>' + Account.address + '</td>';
        list_Account += '<td>' + Account.username + '</td>';
        list_Account += '<td>' + Account.smtp_server + '</td>';

        list_Account += '</tr>';

    });
    $('#list_Account').html(list_Account);

}

// load data
function loadDataAccount() {

    var id_account = $('#data_Account').attr("data-Account");
    getDataJson('/qemail/Account/getOneInJson/' + id_account, 'fillDataAccount()');

}


// fill data detail
function fillDataAccount() {

    var data_json = ' ';
    var detail_title = '';

    $.each(data_response, function (i, Account) {


        data_json += ' <div class="row m-1">';
        data_json += '    <div class="col-sm-3 bg-gray-100 p-2">';
        data_json += '        <b>' + i + '</b>';
        data_json += '    </div>';
        data_json += '    <div class="col-sm-9 border border-1 border-top-0 border-right-0" id="data_' + i + '">';
        data_json += '        ' + Account + '';
        data_json += '    </div>';
        data_json += '</div>';

    });


    $('#detail_title').html(detail_title);
    $('#data_Account').html(data_json);

}