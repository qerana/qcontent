/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  diemarc
 * Created: 09 Mon, 2020
 */

CREATE DATABASE IF NOT EXISTS  `qerana_fw` /*!40100 DEFAULT CHARACTER SET utf8 */;

CREATE TABLE IF NOT EXISTS `qemail_accounts` (
  `id_account` int(11) NOT NULL AUTO_INCREMENT,
  `account` varchar(45) NOT NULL,
  `address` varchar(150) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` text NOT NULL,
  `smtp_server` varchar(150) NOT NULL,
  `smtp_auth` tinyint(1) NOT NULL DEFAULT '1',
  `smtp_port` int(2) NOT NULL DEFAULT '587',
  `from_name` varchar(45) DEFAULT 'kerana mailer',
  `smtp_secure` varchar(20) DEFAULT NULL COMMENT 'tls',
  PRIMARY KEY (`id_account`),
  UNIQUE KEY `mail_address_UNIQUE` (`address`)
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='qemail  mail account settings, used for sending mails.';

CREATE TABLE `qemail` (
  `id_email` int(11) NOT NULL AUTO_INCREMENT,
  `id_account` int(11) NOT NULL,
  `type` varchar(45) NOT NULL DEFAULT '0',
  `destination` text NOT NULL,
  `bcc` text,
  `subject` varchar(45) DEFAULT NULL,
  `body` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '1=enviado,2=error,3=cola',
  `created_by` varchar(200) DEFAULT NULL,
  `sw_status` int(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_email`),
  KEY `idx_id_account` (`id_account`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `qemail_attachments` (
  `id_email_attachment` int(11) NOT NULL AUTO_INCREMENT,
  `id_email` int(11) NOT NULL,
  `path_attachment` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_email_attachment`)
) ENGINE=MyISAM CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `qemail_templates` (
  `id_qemail_tpl` INT NOT NULL AUTO_INCREMENT,
  `tpl_name` VARCHAR(100) NOT NULL,
  `tpl_type` VARCHAR(100) NOT NULL,
  `tpl_content` TEXT NOT NULL,
  `tpl_active` INT NOT NULL DEFAULT 1,
  `tpl_settings` TEXT NULL,
  PRIMARY KEY (`id_qemail_tpl`))
ENGINE = MyISAM;
