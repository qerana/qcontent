<?php

/*
 * Copyright (C) 2019 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Qerapp\qcontent\controller;


defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Test
  |*****************************************************************************
  |
  | Controller Test
  | @author qdevtools,
  | @date 2020-03-16 06:04:53,
  |*****************************************************************************
 */

class TestController extends \Qerana\core\QeranaC {


     public function __construct()
    {
        parent::__construct();
    }
    
    
    
    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index():void{
      
        $vars = [];
       \Qerana\core\View::showView('test/index_test', $vars);
    }
    
    
    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    
    public function add() : void{

        $vars = [];
        
        \Qerana\core\View::showForm('test/add_test',$vars);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    
    public function save():void{
        
  
    }
    
    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    
    public function detail(int $id) : void{
        
        $vars = [];
        
         \Qerana\core\View::showView('test/detail_test',$vars);
        
    }
    
    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    
    public function edit(int $id) :void{
        
        $vars = [];
        \Qerana\View::showForm('test/edit_test',$vars);
    }
    
    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    
    public function modify():void{
    }
    
    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    
    public function delete(int $id):void{
    }
    
   

}