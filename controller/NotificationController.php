<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
namespace Qerapp\qcontent\controller;

use Qerapp\qcontent\model\content\NotificationService;


defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Notification
  |*****************************************************************************
  |
  | Controller Notification
  | @author qDevTools,
  | @date 2020-04-20 08:20:24,
  |*****************************************************************************
 */

class NotificationController extends \Qerana\core\QeranaC {

    protected
            $_NotificationService;

     public function __construct()
    {
        parent::__construct();
        $this->_NotificationService = new NotificationService;
    }
    
    
     /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson(){
        return $this->_NotificationService->getAll(true);
    }
    
    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id){
        return $this->_NotificationService->getById($id,true);
    }
    
    
    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index():void{
      
        
        $vars = [
            'Plugins' => [
                 'data_json.js',
                'app/notification.js'
            ]
        ];
      \Qerana\core\View::showView('notification/index_notification', $vars);
    }
    
    
   
    
    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    
    public function add() : void{

        $vars = [];
        
         \Qerana\core\View::showForm('notification/add_notification',$vars);
    }
    
     /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    
    public function save():void{
        
        $this->_NotificationService->save();
  
    }
    
    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    
    public function detail(int $id) : void{
        
        
       $vars = [
            'id_notification' => $id,
            'Notification' => $this->_NotificationService->getById($id),
            'Plugins' => [
                 'data_json.js',
                'app/notification.js'
            ]
        ];
        
          \Qerana\core\View::showView('notification/detail_notification',$vars);
        
    }
    
    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    
    public function edit(int $id) :void{
        
        $vars = [
            'Notification' => $this->_NotificationService->getById($id),
            'Plugins' => [
                 'data_json.js',
                'app/notification.js'
            ]
        ];
        \Qerana\core\View::showForm('notification/edit_notification',$vars);
    }
    
    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    
    public function modify():void{
          $this->_NotificationService->save();
    }
    
    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    
    public function delete(int $id):void{
          $this->_NotificationService->delete($id);
          \helpers\Redirect::toAction('index');
    }
    
   

}