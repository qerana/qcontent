<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\controller;

use Qerapp\qcontent\model\content\PageService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Pagemanagement
  |*****************************************************************************
  |
  | Controller Pagemanagement
  | @author qDevTools,
  | @date 2020-03-12 06:18:44,
  |*****************************************************************************
 */

class PagemanagementController extends \Qerana\core\QeranaC {

    protected
            $_PageService;

    public function __construct() {
        parent::__construct();
        $this->_PageService = new PageService;
    }

    /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson() {
        return $this->_PageService->getAll(true);
    }

    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id) {
        return $this->_PageService->getById($id, true);
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index(): void {



        $vars = [
            'Pages' => $this->_PageService->getAll(),
            'Plugins' => [
                'data_json.js',
                'app/page.js'
            ]
        ];
        \Qerana\core\View::showView('pagemanagement/index_page', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function add(): void {

        $vars = [
            'types' => $this->_PageService->page_types,
            'Pages' => $this->_PageService->getAll(),
        ];

        \Qerana\core\View::showForm('pagemanagement/add_page', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function save(): void {

        $this->_PageService->save();
        \helpers\Redirect::toAction('detail/' . $this->_PageService->Page->id_page);
    }

    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function detail(int $id): void {


        $vars = [
            'id_page' => $id,
            'Page' => $this->_PageService->getById($id),
            'Plugins' => [
                'data_json.js',
                'app/page.js'
            ]
        ];

        \Qerana\core\View::showView('pagemanagement/detail_page', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function edit(int $id): void {

        
        $PageService = new PageService();
        
        $p = $this->_PageService->getById($id);
        $vars = [
            'Page' => $this->_PageService->getById($id),
            'types' => $this->_PageService->page_types,
            'Pages' => $PageService->getAll(),
            'Plugins' => [
                'data_json.js',
                'app/page.js'
            ]
        ];
        
        
        \Qerana\core\View::showForm('pagemanagement/edit_page', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    public function modify(): void {
        $this->_PageService->save();
        \helpers\Redirect::toAction('detail/' . $this->_PageService->Page->id_page);
    }

    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function delete(int $id): void {
        $this->_PageService->delete($id);
        \helpers\Redirect::toAction('index');
    }

}
