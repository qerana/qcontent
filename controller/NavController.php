<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\controller;

use Qerapp\qcontent\model\content\NavService;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Nav
  |*****************************************************************************
  |
  | Controller Nav
  | @author qDevTools,
  | @date 2020-03-07 08:00:33,
  |*****************************************************************************
 */

class NavController extends \Qerana\core\QeranaC {

    protected
            $_NavService;

    public function __construct() {
        parent::__construct();
        $this->_NavService = new NavService;
        //$this->_NavService->runTest();
    }

    /**
     * -------------------------------------------------------------------------
     * Get all in json
     * -------------------------------------------------------------------------
     */
    public function getAllInJson() {
        return $this->_NavService->getAll(true);
    }

    /**
     * -------------------------------------------------------------------------
     * Get one in json
     * -------------------------------------------------------------------------
     */
    public function getOneInJson(int $id) {
        return $this->_NavService->getById($id, true);
    }

    /**
     * -------------------------------------------------------------------------
     * Show all
     * @return void
     * -------------------------------------------------------------------------
     */
    public function index(): void {


        $vars = [
            'Plugins' => [
                'data_json.js',
                'app/nav.js'
            ]
        ];
        \Qerana\core\View::showView('nav/index_nav', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Add new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function add(): void {

        $vars = [
            'Parents' => $this->_NavService->getAll(),
            'targets' => $this->_NavService->targets,
            'positions' => $this->_NavService->positions,
        ];

        \Qerana\core\View::showForm('nav/add_nav', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Save new 
     * @return void
     * -------------------------------------------------------------------------
     */
    public function save(): void {

        $this->_NavService->save();
        \helpers\Redirect::toAction('detail/'.$this->_NavService->Nav->id_nav);
    }

    /**
     * -------------------------------------------------------------------------
     * View
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function detail(int $id): void {


        $vars = [
            'id_nav' => $id,
            'Nav' => $this->_NavService->getById($id),
            'Childs' => $this->_NavService->getNavMenu($id),
            'Plugins' => [
                'data_json.js',
                'app/nav.js'
            ]
        ];


        \Qerana\core\View::showView('nav/detail_nav', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * Edit 
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function edit(int $id): void {

        // new service for get all navs
        $NavService = new NavService();
        
        $vars = [
            'Nav' => $this->_NavService->getById($id),
            'Parents' => $NavService->getAll(),
            'targets' => $this->_NavService->targets,
            'positions' => $this->_NavService->positions,
            'Plugins' => [
                'data_json.js',
                'app/nav.js'
            ]
        ];
        \Qerana\core\View::showForm('nav/edit_nav', $vars);
    }

    /**
     * -------------------------------------------------------------------------
     * modify
     * -------------------------------------------------------------------------
     * @return void
     */
    public function modify(): void {
        $this->_NavService->save();
    }

    /**
     * -------------------------------------------------------------------------
     * Delete
     * -------------------------------------------------------------------------
     * @param int $id
     * @return void
     */
    public function delete(int $id): void {
        $this->_NavService->delete($id);
        \helpers\Redirect::toAction('index/');
    }

}
