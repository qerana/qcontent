<?php

/*
 * Copyright (C) 2019-20 diemarc diemarc@protonmail.com
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace Qerapp\qcontent\controller;

use Qerapp\qcontent\model\content\PageService,
    Qerapp\qcontent\model\content\QContentService AS Content;

defined('__APPFOLDER__') OR exit('Direct access to this file is forbidden, siya');
/*
  |*****************************************************************************
  | CONTROLLER CLASS  Page
  |*****************************************************************************
  |
  | Controller Page
  | @author qDevTools,
  | @date 2020-03-11 06:38:15,
  |*****************************************************************************
 */

class PageController {

    protected
            $_PageService;

    public function __construct() {

        $this->_PageService = new PageService;
    }

    /**
     * show home page
     */
    public function index() {

        $vars = [
            'Page' => $this->_PageService->getPage('home.html')
        ];
        
        Content::show($vars);
    }

    /**
     * View a page
     * @param int $title
     */
    public function view(string $title) {

        try {
            $Page = $this->_PageService->getPage($title);
        } catch (\Exception $ex) {
            \QException\Exceptions::ShowException('Qcontent', $ex);
        }
        

        $vars = [
            'Page' => $Page
        ];

        Content::show($vars);
    }

}
